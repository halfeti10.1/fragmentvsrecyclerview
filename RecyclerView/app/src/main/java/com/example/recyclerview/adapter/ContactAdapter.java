package com.example.recyclerview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.recyclerview.R;
import com.example.recyclerview.model.Contact;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContacViewHolder> {

    private final LayoutInflater mInflater;
    private final ArrayList<Contact> mContactList;
    private Context context;

    public ContactAdapter(Context ctx, ArrayList<Contact> contacts){
        mInflater = LayoutInflater.from(ctx);
        this.mContactList = contacts;
        this.context=ctx;
    }
    @NonNull
    @Override
    public ContacViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false);

        ContacViewHolder vh = new ContacViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ContacViewHolder holder, int position) {
        final Contact contact;
        contact=mContactList.get(position);
        holder.tvName.setText(String.valueOf(contact.getName()));
        holder.tvPhone.setText(String.valueOf(contact.getPhone()));
        Glide.with(context)
                .load(String.valueOf(contact.getUrlImage()))
                .centerCrop()
                .into(holder.ivImg);
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    public class ContacViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvPhone;
        public ImageView ivImg;
        public ContacViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=(TextView)itemView.findViewById(R.id.tv_name);
            tvPhone=(TextView)itemView.findViewById(R.id.tv_phone);
            ivImg= (ImageView)itemView.findViewById(R.id.imageView);
        }
    }
}
