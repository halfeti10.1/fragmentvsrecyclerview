package com.example.recyclerview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class AddContactFragment extends Fragment {
    public static String name, phone, imgUrl;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_add_contact, container, false);

        final EditText edName= (EditText) view.findViewById(R.id.ed_name);
        final EditText edPhone= (EditText) view.findViewById(R.id.ed_phone);
        final EditText edImg= (EditText) view.findViewById(R.id.et_url);
        Button btnAdd= (Button) view.findViewById(R.id.button_add);
        Button btnCancel= (Button) view.findViewById(R.id.button_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                }
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name=edName.getText().toString().trim();
                phone=edPhone.getText().toString().trim();
                imgUrl=edImg.getText().toString().trim();
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                }
//                getChildFragmentManager().popBackStack();
            }
        });


        return view;
    }


    public static AddContactFragment nameInstance() {
        Bundle args = new Bundle();
        args.putString("name",name);
        AddContactFragment fragment = new AddContactFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public static AddContactFragment phoneInstance() {

        Bundle args = new Bundle();
        args.putString("phone", phone);
        AddContactFragment fragment = new AddContactFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public static AddContactFragment imgInstance() {

        Bundle args = new Bundle();
        args.putString("img", imgUrl);
        AddContactFragment fragment = new AddContactFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
