package com.example.recyclerview.model;

public class Contact {
    private String name, phone, urlImage;

    public Contact(String name, String phone, String urlImage){
        this.name=name;
        this.phone=phone;
        this.urlImage=urlImage;
    }
    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
