package com.example.recyclerview;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.recyclerview.adapter.ContactAdapter;
import com.example.recyclerview.model.Contact;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;

import static com.example.recyclerview.AddContactFragment.imgInstance;
import static com.example.recyclerview.AddContactFragment.nameInstance;
import static com.example.recyclerview.AddContactFragment.phoneInstance;

public class ContactFragment extends Fragment {
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView recyclerView;
    private String newName, newPhone, newImg;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        View view= inflater.inflate(R.layout.fragment_main, container, false);

        final Button fab = view.findViewById(R.id.fab);

        //get data from add contact
        if (nameInstance().getArguments()!=null){
            newName= Objects.requireNonNull(nameInstance().getArguments()).getString("name", null);
        }
        if (phoneInstance().getArguments()!=null){
            newPhone= Objects.requireNonNull(phoneInstance().getArguments()).getString("phone", null);
        }
        if (imgInstance().getArguments()!=null){
            newImg= Objects.requireNonNull(imgInstance().getArguments()).getString("img", null);
        }

        //recyclerview for contacts
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_contact );
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        // specify an adapter
        final ArrayList<Contact> contacts = new ArrayList<Contact>();
        addContact(contacts);
        if (newName!= null && newPhone!= null){
            Contact newContact = new Contact(newName, newPhone, newImg);
            contacts.add(newContact);
        }
        mAdapter = new ContactAdapter(getActivity(),contacts);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);

        //fab
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AddContactFragment fragment = new AddContactFragment();
                fragmentTransaction.add(R.id.add_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                fab.setVisibility(View.GONE);
            }
        });

        final SwipeRefreshLayout pullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                pullToRefresh.setRefreshing(false);
            }
        });
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    private void refreshData() {
        FragmentTransaction ft = null;
        if (getFragmentManager() != null) {
            ft = getFragmentManager().beginTransaction();
        }
        if (Build.VERSION.SDK_INT >= 26) {
            ft.setReorderingAllowed(false);
        }
        ft.detach(this).attach(this).commit();
    }

    private void addContact(ArrayList<Contact> contacts) {
        contacts.add(new Contact("Tat", "1245679", "https://i.pinimg.com/originals/3c/a8/ed/3ca8ed8b7a51711b60d2e79ee2ff0cfb.jpg"));
        contacts.add(new Contact("Chuong", "12485679", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT54fCHhv-m0SbfNpK-MwrgKhmWoBEgbXnkh17RAaiBhVOoy6rS&s"));
        contacts.add(new Contact("Dien", "124567d9", "https://a.wattpad.com/cover/164036100-288-k39994.jpg"));
        contacts.add(new Contact("Thoai", "12454679", "https://i.pinimg.com/236x/51/fc/34/51fc34b8d41c89f14be7e4a96cc52b88--ram-zero.jpg"));
        contacts.add(new Contact("Trong", "12425679", "https://i.pinimg.com/originals/6a/21/d7/6a21d703227b9962a6c6b0a2195c06f4.jpg"));
        contacts.add(new Contact("Gio", "12456179", "https://i.pinimg.com/736x/00/80/80/00808062d2af4ccbb9ba617d92cde5e2.jpg"));
        contacts.add(new Contact("Lam", "12456779", "https://i.pinimg.com/originals/50/dd/28/50dd280b800176b5a7af13376a21acdb.jpg"));
        contacts.add(new Contact("Viec", "12456079", "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSAjg6DK1Y9ycE0oA8rFz2kgye6y7A8ELLM6Q&usqp=CAU"));
//        https://i.pinimg.com/236x/3f/07/38/3f07386bf9e06cd91bc3e7ffaab1663f--rem-ram-zero.jpg

    }



}
